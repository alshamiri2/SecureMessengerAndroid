package com.gitlab.bettehem.securemessengerandroid.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager
import com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels.ProfileManagerViewModel
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment_layout.*
import kotlinx.android.synthetic.main.new_profile_layout.*

//This is the main fragment of the app
class MainFragment : Fragment() {

    private val MAIN_VIEW = 0
    private val PROFILE_CREATOR_VIEW = 1

    private lateinit var profileManagerViewModel: ProfileManagerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //attach ViewModel
        profileManagerViewModel = ViewModelProviders.of(activity!!).get(ProfileManagerViewModel::class.java)

        //check if user has a profile. If not, show profile creation screen
        if (ProfileManager().profileCreated(activity)) {
            mainViewFlipper.displayedChild = MAIN_VIEW
        } else {
            mainViewFlipper.displayedChild = PROFILE_CREATOR_VIEW
        }


        //setup buttons
        buttons()

        val profileAddedObserver: Observer<ProfileManager.UserProfile> = Observer { profile ->
            //check if profile was created successfully
            if (ProfileManager().profileCreated(activity)){
                //set ViewFlipper to show main view
                mainViewFlipper.displayedChild = MAIN_VIEW
            }else{
                //set ViewFlipper to show profile creator view
                mainViewFlipper.displayedChild = PROFILE_CREATOR_VIEW
            }
        }
        profileManagerViewModel.getAddedProfile().observe(this, profileAddedObserver)

    }

    //called when the fragment is resumed
    override fun onResume() {
        super.onResume()
        //uncheck the navigation drawer's item(s)
        activity!!.findViewById<NavigationView>(R.id.nav_view).menu.getItem(0).isChecked = false
    }

    private fun buttons() {
        //TODO: start a new chat when the newChatButton is pressed
        newChatButton.setOnClickListener { }
        //TODO: allow deletion of chats when newChatButton is pressed
        newChatButton.setOnLongClickListener { false }


        saveProfileButton.setOnClickListener {
            //check if user has typed a username and only then attempt to save a profile
            if (newUsernameEditText.text!!.isNotEmpty()){
                //TODO: don't accept spaces in usernames
                //TODO: subscribe to topic
                //save profile
                profileManagerViewModel.saveNewProfile(newUsernameEditText.text.toString())
            }
        }
    }

}
